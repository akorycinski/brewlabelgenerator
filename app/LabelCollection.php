<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;
use App\Label;
/**
 * Description of LabelCollection
 *
 * @author adam
 */
class LabelCollection {

    private $_filePath;
    private $collection = array();

    public function __construct($filePath) {
        $this->_filePath = $filePath;
        $this->_readFile();
    }

    private final function _readFile() {
        $inputString = file_get_contents(__DIR__ . "/../" . $this->_filePath);
        if (!$inputString) {
            throw new Exception("File not found.\n");
        }
        $json_decode = json_decode($inputString);

        foreach ($json_decode as $item) {
            foreach ($item->amount as $volume => $amount) {
                $label = new Label($item->name, $item->number_brew, $item->type, $item->blg, $item->abv, $item->ibu, $item->ebc, $item->kcal, $item->ingredients, $item->date_brew, $item->date_bottling, $volume, $amount);
                $this->collection[] = $label;
            }
        }

        
        foreach ($this->collection as $value) {
            echo $value;
        }
        echo "Data Loaded.\n";
        
    }
    
    public function get(){
        if(count($this->collection)==0){
            return false;
        }
        
        $item = $this->collection[0];
        $get = $item->get();
        
        if($get){
            return $get; 
        }
        array_shift($this->collection);
        return $this->get();
        
    }

}
