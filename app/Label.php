<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

/**
 * Description of Label
 *
 * @author adam
 */
class Label {

    public $name;
    public $number_brew;
    public $type;
    public $blg;
    public $abv;
    public $ibu;
    public $ebc;
    public $kcal;
    public $ingredients;
    public $dateBrew;
    public $dateBottling;
    public $volume;
    public $amount;

    function __construct($name, $number_brew, $type, $blg, $abv, $ibu, $ebc, $kcal, $ingredients, $dateBrew, $dateBottling, $volume, $amount) {
        $this->name = $name;
        $this->number_brew = $number_brew;
        $this->type = $type;
        $this->blg = $blg;
        $this->abv = $abv;
        $this->ibu = $ibu;
        $this->ebc = $ebc;
        $this->kcal = $kcal;
        $this->ingredients = $ingredients;
        $this->dateBrew = $dateBrew;
        $this->dateBottling = $dateBottling;
        $this->volume = $volume;
        $this->amount = $amount;
    }

    public function __toString() {
        return $this->name . " - " . $this->amount . " etykiet dla " . $this->volume . " ml.\n";
    }
    
    public function get(){
        if($this->amount==0){
            echo "Koniec ".$this->name . " - " . $this->volume." ml.\n";
            return false;
        }
        $this->amount--;
        return $this;
    }

}
