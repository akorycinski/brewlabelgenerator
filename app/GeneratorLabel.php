<?php

namespace App;

use TCPDF;
use App\Label,
    App\LabelCollection;

class GeneratorLabel extends TCPDF {

    private $styleLeft = 'style="font-size: 9px; font-family: ttslabscondensedb; text-align:left;"';
    private $styleRight = 'style="font-size: 9px; font-family: ttslabscondensed; text-align:right;"';
    private $_lc;
    private $_inputFile;

    public function __construct($inputFile) {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->_inputFile = explode(".",end(split("/", $inputFile)))[0];explode(".",end(split("/", $inputFile)))[0];
        $this->_lc = new LabelCollection($inputFile);
    }

    function run() {
        $start = microtime(true);
        $this->_generate();

        $finish = microtime(true);

        echo "Wykonano w " . ($finish - $start) . " s.\n";
    }

    private function _generate() {
        $this->setPageOrientation("L");
        $this->setPrintHeader(false);
        $this->setPrintFooter(false);
        $this->SetAutoPageBreak(false);
        $this->SetMargins(5, 5, 5, true);

        $this->AddFont("ttslabscondensed", '', 10, '', false);
        $this->AddFont("ttslabscondensedb", '', 10, '', false);

//        for ($i = 0; $i < 4; $i++) {
//            $this->_generateLabel($i);
//        }
        $i = 0;
        do {

            $get = $this->_lc->get();
            if ($get) {
                $this->_generateLabel($i, $get);
            }
            $i++;
        } while ($get);

        $x = date('Ymd_His');
        $this->Output(__DIR__ . "/../output/" . $this->_inputFile . "_" . $x . '.pdf', 'F');
    }

    private final function _generateLabel($number, Label $label) {
        $offset = $number % 4;
        if ($offset == 0) {
            $this->AddPage();
        }

        //init default position
        $x = 10;
        $y = 5;
        if ($offset > 1) {
            $y = 105;
        }
        if ($offset % 2 == 1) {
            $x = 150;
        }

        $this->_makeLabelBackground($x, $y);
        $this->_makeBrandName($x, $y, "Topór");
        $this->_makeBeerName($x, $y, $label->name);
        $this->_makeStats($x, $y, $label->number_brew, $label->type);
        $this->_makeDetails($x, $y, $label->blg, $label->abv, $label->ibu, $label->ebc, $label->kcal, $label->volume);
        $this->_makeListOfIngredients($x, $y, $label->ingredients);
        $this->_makeImportantDate($x, $y, $label->dateBrew, $label->dateBottling);
//        $this->_makeFooterWithSignature($x,$y,$dateBrew, $dateBottling);
    }

    private function _makeLabelBackground($x, $y) {
        $background = __DIR__ . "/../base/background_with_border_with_logo.png";
        $this->Image($background, $x, $y, 130, 95, "PNG", "", "", true, 600, '', false, false, 0);
    }

    private function _makeBrandName($x, $y, $brandName) {
        $this->SetFont("burfordrusticshadowtwoa", '', 14, '', false);
        $this->writeHTMLCell(
                82, 1, $x + 2, $y + 1, '<div style="font-size: 56px; color: #373736;">' . $brandName . '</div>', 0, 1, false, false, "C", false);
    }

    private function _makeBeerName($x, $y, $beerName) {
        $this->SetFont("burfordrusticshadowtwoa", '', 14, '', false);
        $extraCss = "";
   #     echo strlen($beerName)."\n";
                
        if(strlen($beerName)> 15){
                        $extraCss = " letter-spacing: -3px;";
        }
        $this->writeHTMLCell(82, 1, $x + 2, $y + 78, '<div style="font-size: 31px; color: #373736;'.$extraCss.'">' . $beerName . '</div>', 1, 1, false, false, "C", false);
    }

    /**
     * 
     * @param type $x - pozycja x etykiety
     * @param type $y - pozycja y etykiety
     * @param type $number - numer warki
     * @param type $beerFamily - gatunek piwa
     */
    private function _makeStats($x, $y, $number, $beerFamily) {

        $this->writeHTMLCell(
                42, 1, $x + 85, $y + 4, '<table>'
                . '<tr>'
                . '<td ' . $this->styleLeft . ' colspan="2">Warka nr:</td>'
                . '<td ' . $this->styleRight . '>' . $this->number((string) $number) . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Typ</td>'
                . '<td ' . $this->styleRight . ' colspan="2">' . $beerFamily . '</td>'
                . '</tr>'
                . '</table>', 0, 1, false, false, "R", false);
    }

    private function number($number) {
//        $number = (string)$number;
        $base = "00000" . $number;
        return '#' . (substr($base, strlen($number)));
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $extract
     * @param type $abv
     * @param type $ibu
     * @param type $ebc
     * @param type $kcal
     * @param type $volume
     */
    private function _makeDetails($x, $y, $extract, $abv, $ibu, $ebc, $kcal, $volume) {
        $this->writeHTMLCell(
                42, 1, $x + 85, $y + 15, '<table>'
                . '<tr>'
                . '<td ' . $this->styleLeft . ' >Ekstrakt</td>'
                . '<td ' . $this->styleRight . '>' . $extract . ' BLG</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>ABV</td>'
                . '<td ' . $this->styleRight . '>' . $abv . ' %</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>IBU</td>'
                . '<td ' . $this->styleRight . '>' . $ibu . ' </td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>EBC</td>'
                . '<td ' . $this->styleRight . '>' . $ebc . ' </td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Kalorie</td>'
                . '<td ' . $this->styleRight . '>' . $this->calculateKcal($kcal, $volume) . ' kcal</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '></td>'
                . '<td ' . $this->styleRight . '>' . ' </td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Ilość</td>'
                . '<td ' . $this->styleRight . '>' . $volume . ' ml</td>'
                . '</tr>'
                . '</table>', 0, 1, false, false, "R", false);
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $ingredients
     */
    public function _makeListOfIngredients($x, $y, $ingredients) {
        $this->writeHTMLCell(
                42, 1, $x + 85, $y + 45, '<span style="font-size: 7px; font-family: ttslabscondensed; text-align:justify;">'
                . '<strong>Składniki: </strong><br>'
                . $ingredients . '</span>', 0, 1, false, false, "R", false);
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $dateBrew
     * @param type $dateBottling
     */
    public function _makeImportantDate($x, $y, $dateBrew, $dateBottling) {
        $this->writeHTMLCell(
                42, 1, $x + 85, $y + 65, '<table>'
                . '<tr>'
                . '<td ' . $this->styleLeft . ' >Uwarzono:</td>'
                . '<td ' . $this->styleRight . '>' . $dateBrew . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Rozlano:</td>'
                . '<td ' . $this->styleRight . '>' . $dateBottling . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '></td>'
                . '<td ' . $this->styleRight . '>' . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>piwowar:</td>'
                . '<td ' . $this->styleRight . '>' . '</td>'
                . '</tr>'
                . '</table>', 0, 1, false, false, "R", false);
    }

    /**
     * 
     * @param int $kcal
     * @param int $volume
     */
    public function calculateKcal($kcal, $volume) {
        return (int) ($kcal / 500 * $volume);
    }

}
