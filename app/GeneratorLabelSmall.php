<?php

namespace App;

use TCPDF;
use App\Label,
    App\LabelCollection;

class GeneratorLabelSmall extends TCPDF {

    private $styleLeft = 'style="font-size: 6.5px; font-family: ttslabscondensedb; text-align:left;"';
    private $styleRight = 'style="font-size: 6.5px; font-family: ttslabscondensed; text-align:right;"';
    private $_lc;
    private $_inputFile;

    public function __construct($inputFile) {
        parent::__construct(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->_inputFile = explode(".", end(split("/", $inputFile)))[0];
        explode(".", end(split("/", $inputFile)))[0];
        $this->_lc = new LabelCollection($inputFile);
    }

    function run() {
        $start = microtime(true);
        $this->_generate();

        $finish = microtime(true);

        echo "Wykonano w " . ($finish - $start) . " s.\n";
    }

    private function _generate() {
        $this->setPageOrientation("P");
        $this->setPrintHeader(false);
        $this->setPrintFooter(false);
        $this->SetAutoPageBreak(false);
        $this->SetMargins(5, 5, 5, true);

        $this->AddFont("ttslabscondensed", '', 10, '', false);
        $this->AddFont("ttslabscondensedb", '', 10, '', false);

        $i = 0;
        do {

            $get = $this->_lc->get();
            if ($get) {
                $this->_generateLabel($i, $get);
            }
            $i++;
        } while ($get);

        $x = date('Ymd_His');
        $this->Output(__DIR__ . "/../output/" . $this->_inputFile . "_" . $x . '.pdf', 'F');
    }

    private final function _generateLabel($number, Label $label) {
        $offset = $number % 12;
        if ($offset == 0) {
            $this->AddPage();
        }

        //init default position
        $x = 10;
        $y = 5;
        if ($offset % 2 == 1) {
            $x = 110;
        }

        $_y = floor($offset / 2);
        if ($offset > 1) {
            $y = $_y * 48 + 5;
        }

        $this->_makeLabelBackground($x, $y);
        $this->_makeBrandName($x, $y, "Topór");
        $this->_makeBeerName($x, $y, $label->name);
        $this->_makeStats($x, $y, $label->number_brew, $label->type);
        $this->_makeDetails($x, $y, $label->blg, $label->abv, $label->ibu, $label->ebc, $label->kcal, $label->volume);
        $this->_makeListOfIngredients($x, $y, $label->ingredients);
        $this->_makeImportantDate($x, $y, $label->dateBrew, $label->dateBottling);
        $this->_makeCapacity($x, $y, $label->volume);
    }

    private function _makeLabelBackground($x, $y) {
        $background = __DIR__ . "/../base/background_small.png";
        $this->Image($background, $x, $y, 90, 45, "PNG", "", "", true, 600, '', false, false, 0);
    }

    private function _makeBrandName($x, $y, $brandName) {
        $this->SetFont("burfordrusticshadowtwoa", '', 14, '', false);
        $this->writeHTMLCell(
                60, 1, $x + 25, $y, '<div style="font-size: 40px; color: #373736;">' . $brandName . '</div>', 0, 1, false, false, "L", false);
    }

    private function _makeBeerName($x, $y, $beerName) {
        $this->SetFont("burfordrusticshadowtwoa", '', 14, '', false);
        $extraCss = "";
        #     echo strlen($beerName)."\n";

        if (strlen($beerName) > 10) {
            $extraCss = " letter-spacing: -2px;";
        }
        $this->writeHTMLCell(63, 1, $x + 25.5, $y + 14, '<div style="font-size: 22px; color: #373736;' . $extraCss . '">' . $beerName . '</div>', 0, 1, false, false, "L", false);
    }

    /**
     * 
     * @param type $x - pozycja x etykiety
     * @param type $y - pozycja y etykiety
     * @param type $number - numer warki
     * @param type $beerFamily - gatunek piwa
     */
    private function _makeStats($x, $y, $number, $beerFamily) {

        $this->writeHTMLCell(
                26, 1, $x + 64, $y + 4, '<table>'
                . '<tr>'
                . '<td ' . $this->styleLeft . ' colspan="2">Warka nr:</td>'
                . '<td ' . $this->styleRight . ' colspan="2">' . $this->number((string) $number) . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Typ</td>'
                . '<td ' . $this->styleRight . ' colspan="3">' . $beerFamily . '</td>'
                . '</tr>'
                . '</table>', 0, 1, false, false, "R", false);
    }

    private function number($number) {
//        $number = (string)$number;
        $base = "00000" . $number;
        return '#' . (substr($base, strlen($number)));
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $extract
     * @param type $abv
     * @param type $ibu
     * @param type $ebc
     * @param type $kcal
     * @param type $volume
     */
    private function _makeDetails($x, $y, $extract, $abv, $ibu, $ebc, $kcal, $volume) {
        $html = '<span style="text-align: center; font-size: 7px; font-family: ttslabscondensed;">'
                . '<b>Ekstrakt: </b>' . $extract . ' BLG | '
                . '<b>ABV: </b>' . $abv . '% | '
                . '<b>IBU: </b>' . $ibu . '<br>'
                . '<b>EBC: </b>' . $ebc . ' | '
                . '<b>Kalorie: </b>' . $this->calculateKcal($kcal, $volume) . ' kcal'
                . '</span>';
//        echo "$html \n";
        $this->writeHTMLCell(50, 1, $x + 18, $y + 26, $html, 0, 0, false, false, "C", false);
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $ingredients
     */
    public function _makeListOfIngredients($x, $y, $ingredients) {
        $this->writeHTMLCell(
                65, 10, $x + 0, $y + 35, '<div style="font-size: 6px; font-family: ttslabscondensed; text-align:justify; bottom: 0;">'
                . '<strong>Składniki: </strong>'
                . $ingredients . '</div>', 0, 1, false, false, "R", false);
    }

    /**
     * 
     * @param type $x
     * @param type $y
     * @param type $dateBrew
     * @param type $dateBottling
     */
    public function _makeImportantDate($x, $y, $dateBrew, $dateBottling) {
        $this->writeHTMLCell(
                26, 1, $x + 64, $y + 22, '<table>'
                . '<tr>'
                . '<td ' . $this->styleLeft . ' >Uwarzono:</td>'
                . '<td ' . $this->styleRight . '>' . $dateBrew . '</td>'
                . '</tr>'
                . '<tr>'
                . '<td ' . $this->styleLeft . '>Rozlano:</td>'
                . '<td ' . $this->styleRight . '>' . $dateBottling . '</td>'
                . '</tr>'
                . '</table>', 0, 1, false, false, "R", false);
    }

    /**
     * 
     * @param int $kcal
     * @param int $volume
     */
    public function calculateKcal($kcal, $volume) {
        return (int) ($kcal / 500 * $volume);
    }

    public function _makeCapacity($x, $y, $volume) {
        $html = '<span style="text-align: right; font-size: 10px; font-family: ttslabscondensed;">'
                . $volume . ' ml'
                . '</span>';
        $this->writeHTMLCell(
                26, 1, $x + 64, $y + 40, $html, 0, 1, false, false, "R", false);
    }

}
